import logging, time, traceback
logging.getLogger().addHandler(logging.FileHandler("error.log"))

class Buffer(object):
  def __init__(self, prefix=""):
    self.data = prefix
  def write(self, unbuffered):
    self.data += unbuffered

def get_timestamp(when=None):
  if when == None:
    when = time.time()
  return time.ctime(when) + " " + time.tzname[time.daylight]

def safe_call(func, args):
  try:
    func(*args)
  except Exception, e:
    if isinstance(e, SystemExit):
      raise
    buffer = Buffer("Exception in function %s at %s:\n"
                               % (func.__name__, get_timestamp()))
    traceback.print_exc(file=buffer)
    logging.getLogger().error(buffer.data)
    # Try to report the error interactively.
    from basic_commands import reply
    if len(args) >= 2 and type(args[0]) == type(args[1]) == str:
      try:
        reply(args[0], args[1], "Ow!  Ohh, man, that didn't feel good.  " \
                                +"Somebody get FunnyMan, quick!")
      except Exception:
        pass
    elif len(args) and type(args[0]) == str:
      try:
        reply('', args[0], "Ow!  Ohh, man, that didn't feel good.  " \
                                +"Somebody get FunnyMan, quick!", all=True)
      except Exception:
        pass

def safe(func):
  return lambda *args: safe_call(func, args)
