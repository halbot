import time
from basic_commands import reply

throttles = {}
count, start_time = range(2)

# Implements throttling of the wrapped function, allowing it to be executed only
# once every seconds_between seconds, but not enforcing this restriction until
# enforce_at usages occur within seconds_between seconds of each other.

# Usage:
# @throttle("web")
# def xkcd(...):
def throttle(group, seconds_between=300, enforce_at=10, quiet=False):
  # This is the function that performs the wrapping of xkcd.
  def throttle_function(func):
    # And this is the function that becomes the wrapped xkcd.
    def throttled(*args, **kwargs):
      throttle_info = throttles.setdefault(group, [0, 0])
      current_time = time.time()

      # If we're more than seconds_between seconds after the start of this
      # cluster, we drop old occurances and increase the start_time to match.
      if throttle_info[start_time] + seconds_between < current_time:
        drop_num = (current_time - throttle_info[start_time]) // seconds_between
        if drop_num >= throttle_info[count]:
          throttle_info[:] = [0,0]
        else:
          throttle_info[count] -= drop_num
          throttle_info[start_time] += seconds_between * drop_num

      # If we haven't hit our enforcement limit, we allow it.
      if throttle_info[count] < enforce_at:
        throttle_info[count] += 1
        if throttle_info[start_time] == 0:
          throttle_info[start_time] = current_time

        # Run the throttled function.
        return func(*args, **kwargs)
      else:
        # Attempt to report the error.
        if not quiet and type(args[0]) == str and type(args[1]) == str:
          reply(args[0], args[1], "I'm sorry, but my A.O. unit has become overloaded.  Wait a few minutes and ask me again.")

    # Pass the docstring through.
    throttled.__doc__ = func.__doc__
    return throttled
  return throttle_function
