# This class is a modification of module "sched", from the Python
# standard library.  The modifications are Copyright (C) 2008
# FunnyMan3595 (Charlie Nolan); All Rights Rexerved.

# The intent is to add this to the Python standard library, which
# may require a transfer of copyright.  Until such addition occurs,
# special permission is granted to distribute, run, and otherwise use
# this module as though it were part of that library, provided that
# this notice is maintained.

"""A generally useful, thread-safe event scheduler class.

This is *NOT* a drop-in replacement for the sched module.  See
convert_instructions for more information.

Each instance of this class manages its own queue.
The methods of this class are thread-safe and can be called from
other threads while the scheduler is running.  Only one scheduled
event will run at a time, but be careful of race conditions if other
threads work with the same resources.

Each instance is parametrized with three functions, one that is
supposed to return the current time, one that is supposed to
implement a delay, and one that should cause the delay to return
prematurely.  If none are specified, time.time, .condition.wait, and
.condition.notify are used for real-time scheduling.  You can also
implement simulated time by writing your own functions.  This can
also be used to integrate scheduling with STDWIN events; the delay
function is allowed to modify the queue.  Time can be expressed as
integers or floating point numbers, as long as it is consistent.

Note: the delay function should call .condition.release before
any real-time delay occurs, to minimize delays to other threads.

Events are specified by tuples (time, priority, action, argument).
As in UNIX, lower priority numbers mean higher priority; in this
way the queue can be maintained fully sorted.  Execution of the
event means calling the action function, passing it the argument.
The argument must be a tuple, pack single arguments as (argument,).
The action function may be an instance method so it has another way
to reference private data (besides global variables).  Parameterless
functions or methods can be used by omitting the argument when calling
enter or enterabs, which defaults it to ().
"""

# TODO: Fix the description of argument passing.  It's just
# func(*args), so I'm guessing the terminology is left over from when
# that had another meaning.  I've patched it to be more accurate, but
# it's clumsy.

convert_instructions = """If you have been using the sched module for
real-time scheduling, conversion is easy.  Simply change
sched.schedule(time.time, time.sleep) to threadsafe_sched.schedule()
and .run() to .run_once().

If you have been using sched for custom scheduling, your task is
harder.  You need to make four changes:

1. The delay function should call .condition.release before any
real-time delay delay occurs, to minimize delays to other threads.
If you can convert to real-time, you should probably just call
.condition.wait(real_time) instead.

2. You must implement a wake function that causes the delay function
to return prematurely, as soon as is practical.  If you use
.condition.wait in the delay function, you can leave this as the
default (.condition.notify).  Otherwise, you may wish to consider
using a Condition of your own, from module "threading".

3. The delay function must be callable with no arguments, which
should cause it to wait a large amount of "time", preferably until
the wake function is called.  If using .condition.wait, simply call
it with no arguments.

4. Change .run() to .run_once().
"""

# XXX The timefunc and delayfunc should have been defined as methods
# XXX so you can define new kinds of schedulers using subclassing
# XXX instead of having to define a module or class just to hold
# XXX the global state of your particular time and delay functions.

import bisect
from threading import Condition

__all__ = ["scheduler"]

class scheduler:
    def __init__(self, timefunc = None, delayfunc = None, wakefunc = None):
        """Initialize a new instance, passing the time, delay, and
        wake functions

        If time, delay, or wake are not specified, time.time,
        .condition.wait, and .condition.notify are used,
        respectively.

        """
        self.queue = []
        if timefunc:
          self.timefunc = timefunc
        else:
          import time
          self.timefunc = time.time
        self.condition = Condition()
        if delayfunc:
          self.delayfunc = delayfunc
        else:
          self.delayfunc = self.condition.wait
        if wakefunc:
          self.wakefunc = wakefunc
        else:
          self.wakefunc = self.condition.notify
        self.running = False

    def enterabs(self, time, priority, action, argument = ()):
        """Enter a new event in the queue at an absolute time.

        Returns an ID for the event which can be used to remove it,
        if necessary.

        """
        event = time, priority, action, argument
        self.condition.acquire()
        bisect.insort(self.queue, event)
        if self.queue[0] == event:
          self.wakefunc()
        self.condition.release()
        return event # The ID

    def enter(self, delay, priority, action, argument = ()):
        """A variant that specifies the time as a relative time.

        This is actually the more commonly used interface.

        """
        time = self.timefunc() + delay
        return self.enterabs(time, priority, action, argument)

    def cancel(self, event):
        """Remove an event from the queue.

        This must be presented the ID as returned by enter().
        If the event is not in the queue, this raises RuntimeError.

        """
        self.condition.acquire()
        self.queue.remove(event)
        self.condition.release()

    def empty(self):
        """Check whether the queue is empty.

        Note: .empty() and anything that depends on it should be
        surrounded by .condition.acquire() and .condition.release()
        if multiple threads are running.

        """
        return len(self.queue) == 0

    def _maybe_release(self):
        """Internal function."""
        # Tries to release the condition, but suppresses any error.
        try:
          self.condition.release()
        except Exception:
          pass

    def run_once(self):
        """Execute events until the queue is empty.

        If the scheduler is already running, raises ValueError.

        When there is a positive delay until the first event, the
        delay function is called and the event is left in the queue;
        otherwise, the event is removed from the queue and executed
        (its action function is called, passing it the argument).  If
        the delay function returns prematurely, it is simply
        restarted.

        It is legal for both the delay function and the action
        function to to modify the queue or to raise an exception;
        exceptions are not caught but the scheduler's state remains
        well-defined so run() may be called again.

        A questionably hack is added to allow other threads to run:
        just after an event is executed, a delay of 0 is executed, to
        avoid monopolizing the CPU when other threads are also
        runnable.

        """
        self.condition.acquire()
        if self.running:
            raise ValueError, "Scheduler is already running"
        self.running = True
        try:
            while self.queue:
                time, priority, action, argument = self.queue[0]
                now = self.timefunc()
                if now < time:
                    self.delayfunc(time - now)
                else:
                    del self.queue[0]
                    self.condition.release()
                    void = action(*argument)
                    self.condition.acquire()
                    self.delayfunc(0)   # Let other threads run
                self._maybe_release()
                self.condition.acquire()
        finally:
            self._maybe_release()
            self.running = False

    def run_forever(self):
        """Execute events until the program terminates or an
        exception is raised.

        If the scheduler is already running, raises ValueError.

        When the queue is empty, calls delayfunc with no parameter,
        this should delay until wakefunc is called.

        See .run_once for more information.

        """
        try:
            while True:
                self.run_once()
                self.running = True
                self.condition.acquire()
                self.delayfunc()
                self._maybe_release()
        finally:
            self.running = False
            self._maybe_release()
