import urllib2, urllib, re, threading, thread
from HTMLParser import HTMLParser

MAX_DOWNLOAD = 10240 * 2 # KB

class Done(Exception):
  pass

class TitleParser(HTMLParser):
  is_title = False
  def handle_starttag(self, tag, args):
    if tag == "title":
      self.is_title = True
  def handle_endtag(self, tag):
    if tag == "title":
      raise Done
  result = ""
  def handle_data(self, data):
    if self.is_title:
      self.result += data

class FirstONRParser(HTMLParser):
  result = ""
  def handle_starttag(self, tag, args):
    if tag == "a":
      args = dict(args)
      if args.get("class") == "searchlink":
        self.result = args['href']
        raise Done

class FirstGoogleParser(HTMLParser):
  result = ""
  def handle_starttag(self, tag, args):
    if tag == 'a':
      args = dict(args)
      if args.get("class") == "l":
        self.result = args['href']
        raise Done

class GoogleCalcParser(HTMLParser):
  result = ""
  in_calc = False
  def handle_starttag(self, tag, args):
    if tag == "font":
      args = dict(args)
      if args.get("size") == "+1":
        self.in_calc = True
      elif self.in_calc and args.get("size") == "-1":
        raise Done
    elif tag == "sup":
      self.result += "^"
  def handle_data(self, data):
    if self.in_calc and data == "Web":
      self.in_calc = False
    elif self.in_calc:
      self.result += data
  def handle_charref(self, char):
    if char == "215":
      self.result += "x"

end_of = {"'": "'", '"': '"', '[': '\\]', '(': ')', '<': '>'}
def extract_url(message):
  end_chars = " ,!"
  start = message.index('http://')
  if start != 0:
    end_chars += end_of.get(message[start-1], "")
  url = re.split("[%s]" % end_chars, message[start:], 1)[0]
  if url[-1] in '.?':
    url = url[:-1]
  return url

def _get_html(url, d):
  d["result"] = urllib2.urlopen(url).read(MAX_DOWNLOAD)
  d["event"].set()

def get_html(url):
  # This (and _get_html) is an ugly way of adding a timeout to the urllib2 call.
  # Unfortunately, since urllib2 didn't think to give a normal timeout, this is
  # the only option.
  d = {}
  d["event"] = threading.Event()
  d["result"] = " (timed out) "
  thread.start_new_thread(_get_html, (url, d))
  d["event"].wait(3)
  return d["result"]

def do_parse(url, parser):
  if url == " (timed out) ":
    return url
  html = get_html(url)
  if html == " (timed out) ":
    return html
  try:
    parser.feed(html)
  except Done:
    pass
  return parser.result

whitespace = re.compile(r"\s+")

def get_title(url):
  raw_title = do_parse(url, TitleParser())
  if raw_title == " (timed out) ":
    return raw_title
  safe_title = whitespace.sub(" ", raw_title)
  title = safe_title.strip()
  return title

def first_onr(query, comic=None):
  url = 'http://ohnorobot.com/index.pl?s=' + urllib.quote_plus(query)
  if comic:
    url += "&comic=%d" % comic
  return do_parse(url, FirstONRParser())

def first_google(query):
  url = "http://www.google.com/search?q=" + urllib.quote_plus(query)
  return do_parse(url, FirstGoogleParser())

def google_calc(query):
  url = "http://www.google.com/search?q=" + urllib.quote_plus(query)
  return do_parse(url, GoogleCalcParser())

def get_extension(url):
  last_period = url.rfind('.')
  if len(url) - 7 < last_period < len(url) - 1:
    return url[last_period + 1:].lower()

_known_non_webpage_extensions = {'mp4v': 1, 'gz': 1, 'jpeg': 1, 'jar': 1, 'mp4': 1, 'mp3': 1, 'gl': 1, 'mng': 1, 'pcx': 1, 'tz': 1, 'm4v': 1, 'wmv': 1, 'xpm': 1, 'mpg': 1, 'dl': 1, 'mpc': 1, 'cpio': 1, 'lzh': 1, 'bat': 1, 'qt': 1, 'cmd': 1, 'patch': 1, 'pbm': 1, 'nuv': 1, 'tex': 1, 'btm': 1, 'arj': 1, 'mpeg': 1, 'm2v': 1, 'rz': 1, 'ra': 1, 'rm': 1, 'asf': 1, 'flc': 1, 'bz': 1, 'log': 1, 'mka': 1, 'ace': 1, 'midi': 1, 'yuv': 1, 'tbz2': 1, 'pdf': 1, 'com': 1, 'deb': 1, 'tgz': 1, 'tiff': 1, 'pgm': 1, 'ppm': 1, 'tga': 1, 'diff': 1, 'txt': 1, 'rpm': 1, 'ps': 1, 'vob': 1, 'zip': 1, 'gif': 1, 'mkv': 1, 'rmvb': 1, 'wav': 1, 'ogm': 1, 'bmp': 1, 'jpg': 1, 'flac': 1, 'ogg': 1, 'Z': 1, 'png': 1, 'aac': 1, 'fli': 1, 'au': 1, 'xwd': 1, 'z': 1, 'xcf': 1, 'tar': 1, 'taz': 1, 'rar': 1, 'avi': 1, '7z': 1, 'csh': 1, 'mid': 1, 'zoo': 1, 'tif': 1, 'mov': 1, 'bz2': 1, 'exe': 1, 'doc': 1, 'xbm': 1, 'sh': 1}


# Note: non_webpage and is_webpage are NOT inverses.  Unknown URL types will
# return False from both.
def non_webpage(url):
  '''Returns true if the URL's extension is a known non-webpage type.'''
  return get_extension(url) in _known_non_webpage_extensions

_known_webpage_extensions = {'htm': 1, 'html': 1, 'shtml': 1, 'asp': 1, 'pl': 1, 'cgi': 1, 'jsp': 1, 'php': 1}

# Note: non_webpage and is_webpage are NOT inverses.  Unknown URL types will
# return False from both.
def is_webpage(url):
  '''Returns true if the URL's extension is a known webpage type.'''
  return get_extension(url) in _known_webpage_extensions
