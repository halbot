from __future__ import generators
import re

from irclib import irc_lower

import perms, whois
import globals
from globals import commands, private
from safety import safe_call
from common import get_timestamp, resize

waiting_commands = {}

def got_command(who, where, command, args=""):
  from connection import hal
  if not command in commands:
    command = 'raise error'
    args = "I don't understand that."
  waiting = waiting_commands.get(who, [])
  if not waiting:
    (required_perms, do_it, wants_perms) = resize(commands[command], 3)
    if where == private and required_perms <= perms.owner:
      if wants_perms:
        safe_call(do_it, (who, where, args, perms.owner))
      else:
        safe_call(do_it, (who, where, args))
    elif not wants_perms and required_perms <= perms.voice \
         and where in hal.channels:
      if irc_lower(who) in whois.voice_or_better(where):
        safe_call(do_it, (who, where, args))
      else:
        return
    else:
      waiting_commands[who] = [(where, command, args)]
      hal.connection.whois((who,))
  else:
    waiting.append((where, command, args))

def do_commands(who, info):
  for where, command, args in waiting_commands[who]:
    actual_perms = perms.get_perms(who, where, info)
    if command not in commands:
      command = 'raise error'
      args = "I don't understand that."
    (required_perms, do_it, wants_perms) = resize(commands[command], 3)
    if required_perms <= actual_perms:
      if not wants_perms:
        safe_call(do_it, (who, where, args))
      else:
        safe_call(do_it, (who, where, args, actual_perms))
    elif required_perms > perms.voice <= actual_perms:
      reply(who, where, "I'm sorry, %s, I'm afraid I can't do that." % who,
            all = True)
  waiting_commands[who] = []
whois.callback = do_commands

def reply(who, where, what, all=False):
  from connection import hal
  if where == private:
    hal.connection.privmsg(who, what)
  elif all:
    hal.connection.privmsg(where, what)
  else:
    hal.connection.privmsg(where, "%s: %s" % (who, what))

commands['raise error'] = (perms.voice, reply)

def ping(who, where, args):
  "$ping: Asks Hal to reply to you, so that you can tell you're both connected."
  reply(who, where, "Pong!")
commands['ping'] = (perms.voice, ping)

class TestException(Exception): pass

def error(who, where, args):
  "$error: Raises a test error."
  raise TestException, str(args)
commands['error'] = (perms.ircop, error)

def join(who, where, args):
  "Asks Hal to join the channel you're in.  Always implicit, via /join."
  from connection import hal
  hal.connection.join(where)
commands['do join'] = (perms.op, join)

def shutdown(who, where, args):
  "$shutdown: Shuts Hal down."
  from connection import hal
  hal.connection.quit("Daisy, daaaisy...")
  raise SystemExit
commands['shutdown'] = (perms.ircop, shutdown)

def get_time(who, where, args):
  "$time: Asks Hal what time it is, according to him."
  reply(who, where, "Current time is: " + get_timestamp())
commands['time'] = (perms.voice, get_time)

weak_mask_re = re.compile(".+!.+@.+")
def ignore(who, where, args):
  "$ignore <nick>!<user>@<host>: Makes Hal ignore someone."
  if args.lower().strip() == "list":
    banlist = ", ".join([user for user in globals.my_users if globals.my_users[user] == perms.ban])
    reply(who, where, "Currently ignoring: %s" % banlist)
    return
  if not weak_mask_re.search(args):
    reply(who, where, "Please give a full nick!user@host mask.")
    return
  globals.my_users[args] = perms.ban
  reply(who, where, "Done.")
commands['ignore'] = (perms.ircop, ignore)

def unignore(who, where, args):
  "$unignore <nick>!<user>@<host>: Makes Hal stop ignoring someone."
  if args in globals.my_users and globals.my_users[args] == perms.ban:
    globals.my_users[args] = perms.none
    reply(who, where, "Done.")
  else:
    reply(who, where, "I wasn't ignoring them.")
commands['unignore'] = (perms.ircop, unignore)

def set_perm(who, where, args):
  "$set-perm <who> <permission level>: Sets a user's minimum Hal permission level.  The user must be identified with NickServ to gain the extra permissions.  Available levels: none, halfop, op, admin, owner, ircop"
  split = args.split()
  if len(split) != 2:
    reply(who, where, "Usage: $set-perm <who> <permission level>")
    return
  target, level_name = split
  level = perms.Perms.from_name(level_name)
  if level.name != level_name.lower():
    reply(who, where, "Available levels: none, halfop, op, admin, owner, ircop")
    return
  globals.my_users[target] = level
  reply(who, where, "%s's minimum permission level is now %s." % (target, level))
commands["set-perm"] = (perms.ircop, set_perm)

def tell_commands(who, where, args, user_perms):
  "$commands: Shows you all the stuff you can do."
  command_list = [command for command in commands
                            if " " not in command
                            if commands[command][0] <= user_perms]
  command_list.sort()
  commands_str = ", ".join(command_list)
  reply(who, where,
        "Available commands at your permission level: " + commands_str)
commands['commands'] = (perms.voice, tell_commands, "I want perms.")

def get_help(who, where, args):
  "$help <command>: Gets help on a command.  Use $commands to get a list of available commands.  Confused?  Try: $help basic"
  args = args.lower()
  if not args:
    args = "help"
  elif args in ("basic", "usage", "basic usage", "hal"):
    reply(who, where, "If you're used to troggie, I may seem a bit odd, because I don't behave the same way.  The easiest way to think of it is that in every channel, there's a separate copy of me, so what happens in one doesn't affect the others.  The same is true if you PM me, it's your own personal Hal.")
    return
  elif args == "poker":
    reply(who, where, "I announce Triplejack prize tournaments as they begin staging (5 minutes before they start).  See $help subscribe for information on subscribing to them and $help poke-msg if you need me to /msg you instead of saying your name in channel.  Available groups: poker, pp, fandango, laptop, finals, pokernight.")
    return
  elif " " in args:
    reply(who, where, "One command at a time, please.")
    return
  elif args[0] == "$":
    args = args[1:]
  if args not in commands:
    reply(who, where, "I don't have that command.")
    return
  (required_perms, command) = resize(commands[args],2)
  if required_perms <= perms.voice:
    permstr = ""
  else:
    permstr = "  (Requires %s.)" % required_perms
  docstr = getattr(command, '__doc__', None)
  if not docstr:
    docstr = "No help available for this command."
  reply(who, where, docstr + permstr)
commands['help'] = (perms.voice, get_help)

def troggie(func):
  def continue_if_no_troggie(who, where, *args, **kwargs):
    from connection import hal
    if where in hal.channels:
      chan = hal.channels[where]
      if "troggie" not in chan.userdict:
        func(who, where, *args, **kwargs)
  # Pass the docstring through.
  continue_if_no_troggie.__doc__ = func.__doc__
  return continue_if_no_troggie
