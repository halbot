import irclib, ircbot, time

import whois, perms
from globals import DEBUG, nick, server, port, autojoin_channels, commands, \
                    my_users, private, password, kill_channels, maybe_save
from safety import safe
from common import resize
from factoids import nick_re, is_re
from basic_commands import got_command
irclib.DEBUG = DEBUG

hal = ircbot.SingleServerIRCBot([(server, port)], nick, "Halbot")
hal.connect(server, port, nick)
time.sleep(.1)
if password:
  hal.connection.privmsg("NickServ", "identify " + password)
  hal.connection.send_raw("OPER Hal " + password)
  hal.connection.send_raw("UMODE -hs")
for channel in autojoin_channels:
  hal.connection.join(channel)
for channel in kill_channels:
  hal.connection.join(channel)

irc = hal.ircobj
irc.add_global_handler("whoisuser", whois.got_whois)
irc.add_global_handler("nosuchnick", whois.got_whois)
irc.add_global_handler("307", whois.got_registered)
irc.add_global_handler("whoischannels", whois.got_channels)
irc.add_global_handler("whoisoperator", whois.got_servop)
irc.add_global_handler("endofwhois", whois.got_whoend)
irc.add_global_handler("userhost", whois.got_userhost)

#@safe
def got_invite(conn, event):
  who = irclib.nm_to_n(event.source())
  where = event.arguments()[0]
  got_command(who, where, "do join")
got_invite = safe(got_invite)
irc.add_global_handler("invite", got_invite)

def flood_protect(who, where, is_first):
  if is_first:
    hal.connection.privmsg(where, "The Definite Annoyance eViction Engine (D.A.V.E.) project is proud to present: Flood protection!")
  from panic import do_kickban
  user = irclib.nm_to_n(who)
  whois.userhost_callbacks[user] = (do_kickban,
                                    ("Hal", where, "Flooding.", False))
  hal.connection.userhost([user])

last_messages = {}
#@safe
def got_msg(conn, event):
  for user in my_users:
    if irclib.mask_matches(event.source(), user):
      if my_users[user] == perms.ban:
        return
  msg = event.arguments()[0]
  if msg == "ACTION" and len(event.arguments()) > 1:
    msg = "<action>" + event.arguments()[1]
  who = irclib.nm_to_n(event.source())
  if who.count("Serv"):
    return
  (command, args) = resize(msg.split(" ", 1),2)
  if command and command[0] == "$":
    command = command[1:]
    maybe_save()

  if event.eventtype() == 'privmsg':
    where = private
  else:
    where = event.target()
    last_message_info = last_messages.setdefault(where, ["",0])
    if last_message_info[0] == msg:
        last_message_info[1] += 1
        if last_message_info[1] > 3:
            flood_protect(event.source(), where, last_message_info[1] == 4)
    else:
        if last_message_info[1] > 3:
            hal.connection.privmsg(where, "D.A.V.E. complete.  Have a nice day!")
        last_message_info[0] = msg
        last_message_info[1] = 1

    if msg.count("http://"):
      import html
      got_command(who, where, "title implicit", html.extract_url(msg))
    if msg[0] != "$":
      if nick_re.match(msg):
        (me, command, args) = resize(msg.split(" ", 2),3)
        if command in commands:
          pass
        elif is_re.search(msg):
          command = "do is"
          args = is_re.split(msg.split(" ", 1)[1], 1)
          maybe_save()
        else:
          command = "factoid implicit"
          args = msg.split(" ", 1)[1]
      else:
        if command.lower() == "forget":
          command = "forget"
        elif is_re.search(msg):
          command = "do is"
          args = is_re.split(msg, 1)
          maybe_save()
        else:
          command = "factoid implicit"
          args = msg
  got_command(who, where, command, args)
got_msg = safe(got_msg)
irc.add_global_handler("privmsg", got_msg)
irc.add_global_handler("pubmsg", got_msg)

def got_ctcp(conn, event):
  if event.arguments()[0] == "ACTION":
    got_msg(conn, event)
got_ctcp = safe(got_ctcp)
irc.add_global_handler("ctcp", got_ctcp)

def debug_console():
  while DEBUG:
    cmd = sys.stdin.readline().strip()
    if cmd:
      hal.connection.send_raw(cmd)

def process_events():
  irc.process_forever()
