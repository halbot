import atexit, bz2, cPickle, signal, sys, time
import threadsafe_sched, ircbot
import perms

DEBUG = False
nick = "Hal"
server = "localhost"
port = 6667
try:
  pwfile = open('hal.pass')
  password = pwfile.read().strip()
except Exception:
  password = ""
autojoin_channels = ("#casualgameplay", "#hal")
kill_channels = {}

# Dummy where entry for privmsgs.
private = "p"

# For processing yes/no arguments.
yes = ("1", "on", "true", "enable", "yes")
no = ("0", "off", "true", "disable", "no")

scheduler = threadsafe_sched.scheduler()

commands = {}

from ircbot import IRCDict
my_users = IRCDict()
factoid_dbs = IRCDict()
locked_dbs = IRCDict()
factoids_on = IRCDict()
reminder_dbs = IRCDict()
subscription_dbs = IRCDict()
poke_via_msg = IRCDict()
unlower = {}

SAVE_VERSION = 6
from cPickle import dumps as pickle, loads as depickle
import perms
try:
  db = open("hal.db")
  zipped = db.read()
  pickled = bz2.decompress(zipped)
  (version, data) = depickle(pickled)
  if version == SAVE_VERSION:
    (my_users, factoid_dbs, locked_dbs, reminder_dbs, subscription_dbs, \
     unlower, poke_via_msg, factoids_on, kill_channels) = data
  elif version == 5:
    (my_users, factoid_dbs, locked_dbs, reminder_dbs, subscription_dbs, \
     unlower, poke_via_msg, factoids_on) = data
  else:
    sys.exit("Unrecognized database version.")
  db.close()
  del db, zipped, pickled
except IOError:
  pass

autosave_interval = 60*60*24 # 1 day.
next_save = time.time() + autosave_interval
def maybe_save():
  if time.time() > next_save:
    save()

def save(who=None, where=None, args=None):
  global next_save
  next_save = time.time() + autosave_interval

  pickled = pickle((
                    SAVE_VERSION,
                    (my_users, factoid_dbs, locked_dbs, reminder_dbs,
                     subscription_dbs, unlower, poke_via_msg, factoids_on,
                     kill_channels)
                  ))
  zipped = bz2.compress(pickled)
  db = open("hal.db", "w")
  db.write(zipped)
  db.close()
  if who:
    from basic_commands import reply
    reply(who, where, "Database saved.")

# Try to save even when things go to hell in a handbasket.
atexit.register(save)
signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))
signal.signal(signal.SIGHUP, lambda signum, stack_frame: None)
signal.signal(signal.SIGQUIT, lambda signum, stack_frame: sys.exit(0))
signal.signal(signal.SIGINT, lambda signum, stack_frame: sys.exit(0))

commands['save'] = (perms.ircop, save)
