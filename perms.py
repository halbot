class Perms(object):
  symbols = {}
  names = {}
  def __init__(self, level, symbol=None, name=None):
    self.level = level
    self.symbol = symbol
    if symbol:
      Perms.symbols[symbol] = self
    self.name = name
    if name:
      Perms.names[name] = self
  def __cmp__(self, other):
    return self.level - other.level
  def __str__(self):
    if self.name:
      return self.name
    elif self.symbol:
      return self.symbol
    else:
      return str(self.level)
  #@staticmethod
  def from_symbol(symbol):
    return Perms.symbols.get(symbol, Perms(0))
  from_symbol = staticmethod(from_symbol)
  #@staticmethod
  def from_name(name):
    return Perms.names.get(name.lower(), Perms(0))
  from_name = staticmethod(from_name)
ban = Perms(-2, None, "banned")
none = Perms(-1, None, "none")
present = Perms(0, None, "present") # Default for from_symbol, albeit == not is.
voice = Perms(1, "+", "voice")
hop = Perms(2, "%", "halfop")
op = Perms(3, "@", "op")
admin = Perms(4, "&", "admin")
owner = Perms(5, "~", "owner")
ircop = Perms(6, None, "ircop")

def get_perms(who, where, info):
  from globals import private, my_users
  if info.ircop:
    return ircop
  if where == private:
    server_perm = owner
  else:
    server_perm = info.channel_perms.get(where, none)
  if info.registered:
    my_perm = my_users.get(who, none)
  else:
    my_perm = none
  return max(server_perm, my_perm)
