import time
from sets import Set
from irclib import irc_lower

from globals import private

def get_timestamp(when=None):
  if when == None:
    when = time.time()
  return time.ctime(when) + " " + time.tzname[time.daylight]

def resize(list_or_tuple, length):
  '''Creates a new list or tuple of length 'length', using as many elements from
     list_or_tuple as possible and right-padding with "".  Does not modify
     list_or_tuple.'''
  if len(list_or_tuple) < length:
    if type(list_or_tuple) == list:
      return list_or_tuple + [""] * (length - len(list_or_tuple))
    else:
      return list_or_tuple + ("",) * (length - len(list_or_tuple))
  else:
    return list_or_tuple[:length]

def extract(dbs, key, default_maker):
  if key not in dbs:
    default = default_maker()
    dbs[key] = default
  return dbs[key]

def extract_my_db(dbs, who, where, default_maker):
  if where == private:
    key = who
  else:
    key = where
  return extract(dbs, key, default_maker)

def real_where(who, where):
  if where == private:
    return who
  return where

def safeint(intstr):
  try:
    return int(intstr)
  except ValueError:
    return -1

def multiunion(*sets):
  result = Set()
  for s in sets:
    result = result.union(s)
  return result

def ircdict_to_set(my_dict):
  return Set([irc_lower(key) for key in my_dict])
