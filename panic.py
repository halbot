import irclib
from common import resize
from connection import hal, irc
import globals
from globals import commands, private, nick
from basic_commands import reply
import perms
import whois

def panic(who, where, args):
  "$panic: Engages panic mode in the channel.  Hal will set mode +mi, clear all voices, and disable autovoice.  The inverse is $unpanic."
  if where == private:
    reply(who, where, "Command not available in private, send it in a channel.")
    return
  hal.connection.mode(where, "+mi")
  hal.connection.privmsg("ChanServ", "ACCESS %s ADD %s 100" % (where, nick))
  hal.connection.privmsg("ChanServ", "CLEAR %s VOICES" % where)
  hal.connection.privmsg("ChanServ", "ACCESS %s ADD %s 50" % (where, nick))
  hal.connection.privmsg("ChanServ", "LEVELS %s SET AUTOVOICE 30" % where)
commands['panic'] = (perms.op, panic)

def unpanic(who, where, args):
  "$unpanic: Disables $panic mode in the channel.  Hall will set mode -i, voice everyone, and re-engage autovoice.  Note that he leaves +m, since everyone is autovoiced anyway."
  if where == private:
    reply(who, where, "Command not available in private, send it in a channel.")
    return
  hal.connection.mode(where, "-i")
  hal.connection.privmsg("ChanServ", "LEVELS %s SET AUTOVOICE 0" % where)
  if where in hal.channels:
    buffer = []
    buffer_size = 6
    for user in hal.channels[where].userdict:
      if len(buffer) < buffer_size:
        buffer.append(user)
      else:
        hal.connection.mode(where, "+%s %s" % (("v" * buffer_size),
                                                " ".join(buffer)))
        buffer = [user]
    if buffer:
      hal.connection.mode(where, "+%s %s" % (("v" * len(buffer)),
                                              " ".join(buffer)))
commands['unpanic'] = (perms.op, unpanic)

def kickban(who, where, args):
  """$kickban <user> <reason>: Kicks and bans a user from the channel.  The ban is for *!*@<user's host>.  Note that <user> is case-sensitive."""
  if where == private:
    reply(who, where, "Channel-only command.")
    return
  user, reason = resize(args.split(" ", 1), 2)
  whois.userhost_callbacks[user] = (do_kickban, (who, where, reason))
  hal.connection.userhost([user])
commands['kickban'] = (perms.hop, kickban)

def do_kickban(nm, who, where, reason, akick=True):
  if akick:
    hal.connection.privmsg("ChanServ", "ACCESS %s ADD %s 100" % (where, nick))
    hal.connection.privmsg("ChanServ", "AKICK %s ADD *@%s %s (%s)" %
                                   (where, irclib.nm_to_h(nm), reason, who))
    hal.connection.privmsg("ChanServ", "ACCESS %s ADD %s 50" % (where, nick))
  hal.connection.mode(where, "+b *!*@%s" % irclib.nm_to_h(nm))
  hal.connection.kick(where, irclib.nm_to_n(nm), "%s (%s)" % (reason, who))

def server_ban(who, where, args):
  """$server-ban <user> <reason>: Kicks and bans a user from the server.  The ban is for *@<user's host>.  Note that <user> is case-sensitive."""
  user, reason = resize(args.split(" ", 1), 2)
  whois.userhost_callbacks[user] = (do_server_ban, (who, where, reason))
  hal.connection.userhost([user])
commands['server-ban'] = (perms.ircop, server_ban)

def do_server_ban(nm, who, where, reason):
  hal.connection.privmsg("OperServ", "AKILL ADD +0 *@%s %s (%s)" %
                                 (irclib.nm_to_h(nm), reason, who))
  hal.connection.send_raw("KILL %s %s (%s)" % (irclib.nm_to_n(nm), reason, who))

def kill_channel(who, where, args):
  """$kill-channel <reason>: When sent, and whenever anyone joins the channel after, Hal will run /msg OperServ AKILLCHAN KILL +0 <channel> <reason>, causing all non-oper users on the channel to be kicked off the server and permabanned."""
  reason = args
  if where == private:
    reply(who, where, "Channel-only command.")
  elif not reason:
    reply(who, where, "Usage: $kill-channel <reason>")
  else:
    globals.kill_channels[where] = reason
    do_kill_channel(where)
    reply(who, where, "Done.")
commands['kill-channel'] = (perms.ircop, kill_channel)

def unkill_channel(who, where, args):
  if where == private:
    reply(who, where, "Channel-only command.")
  else:
    if where in globals.kill_channels:
      del globals.kill_channels[where]
      reply(who, where, "Done.")
    else:
      reply(who, where, "Channel was not set to autokill.")
commands['unkill-channel'] = (perms.ircop, unkill_channel)

def do_kill_channel(channel):
  reason = globals.kill_channels[channel]
  hal.connection.privmsg("OperServ", "AKILLCHAN KILL +0 %s %s" % (channel, reason))

def got_join(conn, event):
  channel = event.target()
  if channel in globals.kill_channels:
    do_kill_channel(channel)
irc.add_global_handler("join", got_join)
