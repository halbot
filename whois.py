from __future__ import generators
from sets import Set

import irclib

import perms
from common import multiunion, ircdict_to_set
from safety import safe

def voice_or_better(channel):
  from connection import hal
  if channel in hal.channels:
    chan = hal.channels[channel]
    dicts = (chan.voiceddict, chan.hopdict, chan.operdict, chan.admindict,
             chan.ownerdict)
    return multiunion(*[ircdict_to_set(d) for d in dicts])
  else:
    return Set()

def callback(who, info):
  raise ValueError, "Whois callback never got replaced."

class Whois(object):
  registered = False
  ircop = False
  channel_perms = None
  def __init__(self):
    self.channel_perms = {}

whois_info = {}

#@safe
def got_whois(conn, event):
  user = event.arguments()[0]
  whois_info[user] = Whois()
got_whois = safe(got_whois)

#@safe
def got_registered(conn, event):
  args = event.arguments()
  if len(args) == 2 and args[1] == "is a registered nick":
    user = args[0]
    whois_info[user].registered = True
got_registered = safe(got_registered)

#@safe
def got_channels(conn, event):
  args = event.arguments()
  if len(args) == 2:
    user = args[0]
    channels = args[1].split()
    for c in channels:
      if c[0] != "#":
        whois_info[user].channel_perms[c[1:]] = perms.Perms.from_symbol(c[0])
      else:
        whois_info[user].channel_perms[c] = perms.present
got_channels = safe(got_channels)

#@safe
def got_servop(conn, event):
  args = event.arguments()
  if len(args) == 2:
    user = args[0]
    whois_info[user].ircop = True
got_servop = safe(got_servop)

#@safe
def got_whoend(conn, event):
  who = event.arguments()[0]
  callback(who, whois_info[who])
  if False:
    user = whois_info[who]
    print "Who info for %s:" % who
    print "Registered: %s" % user.registered
    print "IRCop: %s" % user.ircop
    p = user.channel_perms
    perms = "".join([("%s: %s, " % (k, p[k])) for k in p])[:-2]
    print "Channel info: %s" % perms
got_whoend = safe(got_whoend)

userhost_callbacks = {}

#@safe
def got_userhost(conn, event):
  raw = event.arguments()[0]
  nm = raw.strip().replace("=+", "!").replace("=-", "!")
  nick = irclib.nm_to_n(nm)
  if nick in userhost_callbacks:
     func, extra_args = userhost_callbacks[nick]
     func(nm, *extra_args)
got_userhost = safe(got_userhost)
