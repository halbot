#!/usr/bin/env python
from __future__ import generators
import re, ircbot

import perms
from globals import commands, nick, factoid_dbs, locked_dbs, factoids_on, yes, no
from basic_commands import reply, troggie
from common import extract_my_db, resize, real_where

is_re = re.compile(" (is|are|was|were)( |$)", re.IGNORECASE)
nick_re = re.compile(nick + "[:,] ", re.IGNORECASE)

def get_fact_dbs(who, where):
  factoids = extract_my_db(factoid_dbs, who, where, ircbot.IRCDict)
  locked = extract_my_db(locked_dbs, who, where, ircbot.IRCDict)
  return (factoids, locked)

def are_factoids_on(who, where):
  return extract_my_db(factoids_on, who, where, lambda: True)
factoids_are_on = are_factoids_on

def do_is(who, where, args):
  "Handles implicit factoid setting."
  if not factoids_are_on(who, where):
    return
  (key, to_be, garbage, fact) = args
  (factoids, locked) = get_fact_dbs(who, where)
  if key.endswith('?'):
    key = key[:-1]
  if locked.get(key):
    #reply(who, where, "I'm sorry, %s, I'm afraid I can't do that." % who, all=True)
    return
  elif fact:
    factoids[key] = (to_be, fact)
  elif key in factoids:
    del factoids[key]
do_is = troggie(do_is)
commands['do is'] = (perms.voice, do_is)

def forget(who, where, args):
  "$forget <prompt>: Asks Hal to forget his factoid for the given prompt.  For compatibility with troggie, you can omit the $."
  if not factoids_are_on(who, where):
    return
  (factoids, locked) = get_fact_dbs(who, where)
  key = args
  if locked.get(key):
    reply(who, where, "I'm sorry, %s, I'm afraid I can't do that." % who, all=True)
  elif key in factoids:
    del factoids[key]
    reply(who, where, "I forgot %s." % key)
  else:
    reply(who, where, "I don't have anything matching '%s'." % key)
forget = troggie(forget)
commands['forget'] = (perms.voice, forget)

def force(who, where, args):
  "$force <prompt> (is/was/are/were) <something>: Sets a factoid, even if it's locked."
  (factoids, locked) = get_fact_dbs(who, where)
  if not is_re.search(args):
    reply(who, where, "Syntax: $force a (is/are/was/were) b")
    return
  (key, to_be, garbage, fact) = resize(is_re.split(args, 1),4)
  if key.endswith('?'):
    key = key[:-1]
  if fact:
    factoids[key] = (to_be, fact)
  elif key in factoids:
    del factoids[key]
commands['force'] = (perms.op, force)

def lock(who, where, args):
  "$lock <prompt>: Locks Hal's factoid for a given prompt."
  (factoids, locked) = get_fact_dbs(who, where)
  if args.endswith('?'):
    args = args[:-1]
  if not locked.get(args):
    locked[args] = True
    reply(who, where, "Done.")
  else:
    reply(who, where, "It's already locked.")
commands['lock'] = (perms.op, lock)

def unlock(who, where, args):
  "$unlock <prompt>: Unlocks the factoid for a given prompt."
  (factoids, locked) = get_fact_dbs(who, where)
  if args.endswith('?'):
    args = args[:-1]
  if locked.get(args):
    locked[args] = False
    reply(who, where, "Done.")
  else:
    reply(who, where, "It's not locked.")
commands['unlock'] = (perms.op, unlock)

def factoids(who, where, args):
  "$factoids <command>: Administrative functions for Hal's factoids system.  enable - Enables factoids.  disable - Disables factoids.  clear - Clears all factoids and factoid locks."
  (factoids, locked) = get_fact_dbs(who, where)
  args = args.lower()
  on = extract_my_db(factoids_on, who, where, lambda: True)
  if args in yes:
    if on:
      reply(who, where, "Factoids are already enabled.")
    else:
      factoids_on[real_where(who, where)] = True
      reply(who, where, "Factoids enabled.")
  elif args in no:
    if not on:
      reply(who, where, "Factoids are already disabled.")
    else:
      factoids_on[real_where(who, where)] = False
      reply(who, where, "Factoids disabled.")
  elif args == "clear":
    factoids.clear()
    locked.clear()
    reply(who, where, "Factoids (and locks) cleared.")
  else:
    reply(who, where, "Available factoid management tools: enable, disable, clear")
commands['factoids'] = (perms.admin, factoids)

def factoid(who, where, args, implicit=False):
  "$fact <prompt> or $factoid <prompt>: Asks Hal to look up his factoid for the given prompt."
  (factoids, locked) = get_fact_dbs(who, where)
  if args.endswith('?'):
    args = args[:-1]
  if args in factoids:
    (to_be, fact) = factoids[args]
    if not implicit and fact.count("$who"):
      implicit = True
    response = ""
    lfact = fact.lower()
    if lfact.startswith("<"):
      if lfact.startswith("<raw>"):
        response = fact[5:].lstrip()
      elif lfact.startswith("<reply>"):
        response = fact[7:].lstrip()
      elif lfact.startswith("<action>"):
        response = "ACTION %s" % fact[8:].lstrip()
    if not response:
      response = "I hear that %s %s %s" % (args, to_be, fact)
    response = response.replace("$who", who)
    reply(who, where, response, all=implicit)
  elif not implicit:
    reply(who, where, "I don't have anything matching '%s'." % args)
commands['fact'] = (perms.voice, factoid)
commands['factoid'] = (perms.voice, factoid)

def factoid_implicit(who, where, args):
  "Implicit form of $fact[oid]."
  if factoids_are_on(who, where):
    factoid(who, where, args, implicit=True)
factoid_implicit = troggie(factoid_implicit)
commands['factoid implicit'] = (perms.voice, factoid_implicit)
