from __future__ import generators
import re, time, os
from sets import Set
from dateutil.parser import parse as raw_time_parser

import whois, perms
from common import get_timestamp, resize, extract_my_db, real_where, safeint, \
                   extract
from basic_commands import reply
from connection import hal
from globals import reminder_dbs, subscription_dbs, scheduler, private, \
                    commands, unlower, poke_via_msg, yes, no
from safety import safe
from ircbot import IRCDict, irc_lower

os.environ['TZ'] = "GMT"
time.tzset()

def parse_time(time_string):
  return time.mktime(raw_time_parser(time_string).timetuple())

def get_reminder_db(who, where):
  return extract_my_db(reminder_dbs, who, where, lambda: [])

def get_subscription_db(who, where):
  return extract_my_db(subscription_dbs, who, where, IRCDict)

def describe_reminder(reminder):
  (when, message, groups, repeat) = reminder
  description = '"%s" at %s.' % (message, get_timestamp(when))
  if groups:
    groupstr = ",".join(groups)
    description += "  Groups: " + groupstr
  if repeat:
    hours = repeat / 3600
    minutes = str(repeat / 60 % 60).zfill(2)
    description += "  Repeating every %d:%s." % (hours, minutes)
  return description

reminder_res = {"add": re.compile(r'^"((?:[^"]|\")*)" +at +([^"]*)$',
                                  re.IGNORECASE),
                "set": re.compile(r'^(\d+) +(\w+) +(.*)$', re.IGNORECASE),
                "repeat h+:mm": re.compile(r'^(\d+) +every +(\d+):(\d\d)$',
                                           re.IGNORECASE),
                "repeat x units": re.compile(r'^(\d+) +every +(\d+) +' +
                                             r'(week|day|hour|minute)s?$',
                                             re.IGNORECASE),
                "repeat off": re.compile(r'^(\d+) +(off|disable|stop|never|none'
                                        +r'|remove)$',
                                         re.IGNORECASE)
               }

time_units = {"minute": 60,
              "hour": 60*60,
              "day": 60*60*24,
              "week": 60*60*24*7}

def reminder_index(who, where, reminders, whichstr):
  which = safeint(whichstr)
  if which < 1:
    reply(who, where, "Which reminder?")
    return -1
  if which > len(reminders):
    reply(who, where, "I don't have that many reminders.")
    return -1
  return which - 1

def fire_reminder(where, reminders, reminder):
  (when, message, groups, repeat) = reminder
  curtime = time.time()
  if when < (curtime - 60):
    delay = curtime - when
    reply('', where, "Reminder delayed by %d minutes: %s" % (delay // 60,
          message), all=True)
  else:
    delay = 0
    reply('', where, message, all=True)
  if where in hal.channels:
    subscriptions = get_subscription_db("", where)
    please_inform_us = Set()
    for group in groups:
      if group in subscriptions:
        please_inform_us = please_inform_us.union(subscriptions[group])
    informable = whois.voice_or_better(where)
    inform_us = informable.intersection(please_inform_us)
    poke_list = [unlower.get(who, who) for who in inform_us
                                         if not poke_via_msg.get(who, False)]
    if poke_list:
      poke_str = ", ".join(poke_list)
      reply("", where, poke_str + ": I'm in yer reminders, informin' you.",
            all=True)
    for who in inform_us:
      if poke_via_msg.get(who, False):
        reply(who, private, "(%s) %s" % (where, message))
  if repeat:
    if repeat < delay:
      skip = delay // repeat
      reply('', where, "(skipping %d delayed repititions)" % skip, all=True)
      real_repeat = (skip + 1) * repeat
    else:
      real_repeat = repeat
    reminder[0] += real_repeat
    schedule_reminder(where, reminders, reminder)
  else:
    reminders.remove(reminder)
fire_reminder = safe(fire_reminder)

def schedule_reminder(where, reminders, reminder):
  scheduler.enterabs(reminder[0], 0, fire_reminder, (where, reminders, reminder))

def add_reminder(who, where, reminders, reminder):
  if len(reminders) < 10 or where == "#casualgameplay":
    reminders.append(reminder)
    schedule_reminder(real_where(who, where), reminders, reminder)
    reply(who, where, "Reminder %d added." % len(reminders))
  else:
    reply(who, where,
          "I'm sorry, %s, I'm afraid I can't do that.  You're limited to 10."
                    % who)

def cancel_reminder(where, reminders, reminder):
  try:
    scheduler.cancel((reminder[0], 0, fire_reminder, (where, reminders, reminder)))
  except ValueError:
    pass

def reschedule(who, where, reminders, reminder, when):
  rwhere = real_where(who, where)
  cancel_reminder(rwhere, reminders, reminder)
  reminder[0] = when
  schedule_reminder(rwhere, reminders, reminder)

def reminder(who, where, reminder_args):
  "$reminder is so complex it has its own help system: $reminder help"
  (command, args) = resize(reminder_args.split(" ", 1), 2)
  reminders = get_reminder_db(who, where)
  if not command:
    reply(who, where, "Available reminder commands: add del list set repeat help")
  elif command in ("add", "new", "create", "make"):
    parsed = reminder_res["add"].search(args)
    if not parsed:
      reply(who, where, "I don't understand that.  Try $reminder help add")
      return
    (msg, whenstr) = parsed.groups()
    try:
      when = parse_time(whenstr)
    except ValueError:
      reply(who, where, "I don't understand that time.")
      return
    reminder = [when, msg, [], 0]
    add_reminder(who, where, reminders, reminder)
  elif command in ("del", "delete", "remove", "rm"):
    which = reminder_index(who, where, reminders, args)
    if which == -1:
      return
    reminder = reminders[which]
    del reminders[which]
    cancel_reminder(real_where(who, where), reminders, reminder)
    reply(who, where, "Deleted reminder %d: %s" %
                                    (which+1, describe_reminder(reminder)))
  elif command in ("list", "show"):
    if len(reminders) > 5:
      reply(who, where, "Listing %d reminders in private " % len(reminders)
                        + "to avoid channel clutter")
      where = private
    if not reminders:
      reply(who, where, "No reminders.")
    index = 1
    for reminder in reminders:
      reply(who, where, "%2d. %s" % (index, describe_reminder(reminder)))
      index += 1
  elif command in ("change", "alter", "set"):
    parsed = reminder_res["set"].search(args)
    if not parsed:
      reply(who, where, "I don't understand that.  Try $reminder help set")
      return
    (whichstr, property, value) = parsed.groups()
    which = reminder_index(who, where, reminders, whichstr)
    if which == -1:
      return
    reminder = reminders[which]
    lproperty = property.lower()
    if lproperty in ("message", "msg"):
      reminder[1] = value
    elif lproperty == "time":
      try:
        when = parse_time(value)
        reschedule(who, where, reminders, reminder, when)
      except ValueError:
        reply(who, where, "I don't understand that time.")
        return
    elif lproperty in ("group", "groups"):
      groups = value.split(",")
      groups = [group.strip() for group in groups]
      reminder[2] = groups
    reply(who, where, "Done.")
  elif command == "repeat":
    parse_hm = reminder_res["repeat h+:mm"].search(args)
    parse_units = reminder_res["repeat x units"].search(args)
    parse_off = reminder_res["repeat off"].search(args)
    parsed = parse_hm or parse_units or parse_off
    if parsed:
      which = reminder_index(who, where, reminders, parsed.groups()[0])
      if which == -1:
        return
      reminder = reminders[which]
    else:
      reply(who, where, "I don't understand that.  Try $reminder help repeat")
      return
    if parse_hm:
      (whichstr, hourstr, minutestr) = parsed.groups()
      hours = safeint(hourstr)
      if hours < 0:
        reply(who, where, "Bad number of hours.")
        return
      minutes = safeint(minutestr)
      if not (0 <= minutes <= 59):
        reply(who, where, "Bad number of minutes.")
        return
      if hours == minutes == 0:
        reply(who, where, "Repeating continuously sounds like a bad idea.")
        return
      reminder[3] = 60 * (minutes + 60 * hours)
      reply(who, where,
            "Reminder number %d now repeating every %d hours and %d minutes."
                         % (which+1,               hours,      minutes))
    elif parse_units:
      (whichstr, numstr, unit) = parsed.groups()
      unit = unit.lower()
      num = safeint(numstr)
      if numstr < 1:
        reply(who, where, "Bad number of %ss." % unit)
        return
      if unit not in time_units:
        reply(who, where, "I don't know that unit.")
        return
      reminder[3] = num * time_units[unit]
      reply(who, where, "Reminder number %d now repeating every %d %ss."
                                    % (which+1,               num, unit))
    else: # parse_off
      reminder[3] = 0
      reply(who, where, "Repeating disabled on reminder %d." % which+1)

  elif command == "help":
    if not args:
      reply(who, where, "To get help on a specific command, type $reminder " \
                        + "help <command> (e.g. $reminder help add).  " \
                        + "Available reminder commands: add del list set "\
                        + "repeat help")
    elif args in ("add", "new", "create", "make"):
      reply(who, where, '$reminder add "<msg>" at <time>:  Adds a new ' \
                        + "reminder.  When <time> comes, I will say <msg>.  A" \
                        + " variety of time formats are supported.  Use $time" \
                        + " to get my current time for comparison.")
    elif args in ("del", "delete", "remove", "rm"):
      reply(who, where, "$reminder del <reminder>: Delete reminder number " \
                        + "<reminder>.");
    elif args in ("change", "alter", "set"):
      reply(who, where, "$reminder set <reminder> <property> <value>: Change" \
                        + " <property> of reminder number <reminder> to " \
                        + "<value>.  Availale properties: message, time, group")
    elif args in ("list", "show"):
      reply(who, where, "$reminder list: Print a list of reminders.  If there" \
                        + " are more than 5, I will reply in private.")
    elif args == "repeat":
      reply(who, where, "$reminder repeat <reminder> every <interval>: " \
                        + "Reminder number <reminder> will be repeated every " \
                        + "<interval>.  Use $reminder repeat <reminder> off to"\
                        + " stop repeating a reminder.")
    elif args == "help":
      reply(who, where, "$reminder help <command>: get help on <command>.  " \
                        + "You know, like you just did.")
  else:
    reply(who, where, "I don't understand that.")
commands['reminder'] = (perms.admin, reminder)
commands['reminders'] = (perms.admin, reminder)

def reminders_list(who, where, args):
  "$reminders-list: Asks Hal for the current list of reminders."
  reminder(who, where, "list")
commands['reminders-list'] = (perms.voice, reminders_list)

def subscribe(who, where, args):
  "$subscribe <groups>: Subscribes you to the (comma-separated) list of reminder groups.  Hal will try to get your attention when a reminder in one of those groups is triggered.  Reminder groups are listed in $reminders-list"
  if where == private:
    reply(who, where, "Don't you think subscribing to reminders that are" +
                      " already in PM is a bit... pointless?")
    return

  unlower[irc_lower(who)] = who
  subscriptions = get_subscription_db(who, where)
  targets = [target.strip() for target in args.split(",")]
  new_subscription = False
  for target in targets:
    subscribers = extract(subscriptions, target, Set)
    user = irc_lower(who)
    if user not in subscribers:
      subscribers.add(user)
      new_subscription = True
  if new_subscription:
    reply(who, where, "Done.")
  else:
    reply(who, where, "You're already subscribed.")
commands['subscribe'] = (perms.voice, subscribe)

def unsubscribe(who, where, args):
  "$unsubscribe <groups>: Unsubscribes you from the (comma-separated) list of reminder groups."
  subscriptions = get_subscription_db(who, where)
  targets = [target.strip() for target in args.split(",")]
  lost_subscription = False
  for target in targets:
    subscribers = extract(subscriptions, target, Set)
    user = irc_lower(who)
    if user in subscribers:
      subscribers.discard(user)
      lost_subscription = True
  if lost_subscription:
    reply(who, where, "Done.")
  else:
    reply(who, where, "You weren't subscribed.")
commands['unsubscribe'] = (perms.voice, unsubscribe)

def poke_msg(who, where, args):
  "$poke-msg (on/off): Normally, Hal pokes you in the channel.  If you turn $poke-msg on, he'll send you a /msg instead.  Note that unlike most commands, this is global and will affect Hal's behavior in all channels."
  args = args.lower()
  state = poke_via_msg.get(who, False)
  if args in yes:
    if state:
      reply(who, where, "Already sending pokes via /msg.")
    else:
      poke_via_msg[who] = True
      reply(who, where, "Done.")
  elif args in no:
    if not state:
      reply(who, where, "I wasn't sending pokes via /msg.")
    else:
      poke_via_msg[who] = False
      reply(who, where, "Done.")
  else:
    reply(who, where, poke_msg.__doc__)
commands['poke-msg'] = (perms.voice, poke_msg)

def run_reminders():
  for where in reminder_dbs.keys():
    db = reminder_dbs[where]
    for reminder in db:
      schedule_reminder(where, db, reminder)
  while True: # Looping in case of errors.
    safe(scheduler.run_forever)()
