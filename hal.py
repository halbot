#!/usr/bin/env python
import thread

import globals, connection, whois, basic_commands, web, factoids, reminders, panic

from perms import Perms

from connection import debug_console, process_events
from reminders import run_reminders
thread.start_new_thread(debug_console, ())
thread.start_new_thread(run_reminders, ())
if __name__ == "__main__":
  process_events()
else:
  thread.start_new_thread(process_events, ())
