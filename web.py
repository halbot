import html, perms
from basic_commands import reply, troggie
from globals import commands
from throttle import throttle

web_throttle = throttle("web")

def title(who, where, args, all=False):
  "$title <url>: Asks Hal to fetch the title of a given URL.  Not very useful in a channel, where it's done implicitly whenever someone says a URL."
  if html.non_webpage(args):
    return
  try:
    reply(who, where, "[%s]" % (html.get_title(args)), all)
  except Exception, e:
    #if not html.is_webpage(args):
      reply(who, where, "Error retrieving URL '%s'." % args, all)
title = troggie(title)
title = throttle("web", quiet=True)(title)
commands['title'] = (perms.voice, title)

def title_implicit(who, where, args):
  "Implicit version of $title."
  title(who, where, args, all=True)
commands['title implicit'] = (perms.voice, title_implicit)

def calc(who, where, args):
  "$calc <math>: Asks Google's calculator to do some math for you."
  try:
    reply(who, where, "Google says: %s" % html.google_calc(args), all=True)
  except Exception, e:
    reply(who, where, "Ewwww... Google barfed on me.", all=True)
#calc = troggie(calc)
calc = web_throttle(calc)
commands['calc'] = (perms.voice, calc)

def google(who, where, args):
  "$google <search>: Asks Google for its first result for a given search."
  try:
    url = html.first_google(args)
    reply(who, where, "Google says: %s [%s]." % (url, html.get_title(url)), all=True)
  except Exception, e:
    reply(who, where, "Ewwww... Google barfed on me.", all=True)
#google = troggie(google)
google = web_throttle(google)
commands['google'] = (perms.voice, google)

def jig(who, where, args):
  "$jig <search>: Ask Google for its first result for a given search, restricted to Jayisgames."
  google(who, where, args + " site:jayisgames.com")
#jig = troggie(jig)
commands['jig'] = (perms.voice, jig)

def wp(who, where, args):
  "$wp <search> or $wikipedia <search>: Asks Google for its first result for a given search, restricted to Wikipedia."
  google(who, where, args + " site:en.wikipedia.org")
commands['wp'] = (perms.voice, wp)
commands['wikipedia'] = (perms.voice, wp)

def youtube(who, where, args):
  "$youtube <search>: Asks Google for its first result for a given search, restricted to YouTube."
  google(who, where, args + " site:youtube.com")
commands['youtube'] = (perms.voice, youtube)

def onr(who, where, args, comic=None):
  "$onr <search>: Searches OhNoRobot.com, returns the first result."
  try:
    url = html.first_onr(args, comic)
    reply(who, where, "OhNoRobot.com says: %s [%s]." % (url, html.get_title(url)), all=True)
  except Exception, e:
    reply(who, where, "Oh no!  No luck.", all=True)
onr = web_throttle(onr)
commands['onr'] = (perms.voice, onr)

def xkcd(who, where, args):
  "$xkcd <search>: Searches XKCD comics with OhNoRobot.com."
  onr(who, where, args, 56)
commands['xkcd'] = (perms.voice, xkcd)

def weather(who, where, args):
  "$weather <zipcode or location>: Asks Hal for the weather in a given location.  Unfortunately, he doesn't know how yet."
  reply(who, where, "I'm sorry, troggie isn't here, and I don't know how to do that.")
weather = troggie(weather)
commands['weather'] = (perms.voice, weather)

def reset_throttle(who, where, args):
  "$reset-throttle: Resets Hal's throttling mechanism for web requests."
  reply(who, where, "Throttling reset.")
reset_throttle = throttle("web", seconds_between=.1)(reset_throttle)
commands['reset-throttle'] = (perms.ircop, reset_throttle)
